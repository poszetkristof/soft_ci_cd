const express = require('express'),
  bodyParser = require('body-parser'),
  apiProba = require('./api_proba');

const router = express.Router();

router.use(bodyParser.json());
router.use('/belep', apiProba);

module.exports = router;
