const express = require('express'),
  path = require('path'),
  bodyParser = require('body-parser'),
  apiRoutes = require('./api');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('port', process.env.PORT || 3000);

app.use('/api', apiRoutes);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  res.send('Hello, ez itt egy egyszeru fooldal.');
});

app.listen(app.get('port'));
console.log('Server listening on port 3000...');

module.exports = app;
