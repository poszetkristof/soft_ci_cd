FROM node:8.10.0

WORKDIR /soft_ci_cd/

COPY package.json .

RUN npm install --production

COPY . .

EXPOSE 3000

CMD ["node", "app.js"]
